/**
 *
 * @author 朱雀
 */
Ext.define('Skill.structure.Init', {
	mixins: {
		ajax: 'Skill.util.Ajax'
	},
	constructor: function(config) {
		var me = this;
		if (config) {
			Ext.apply(me, config);
		}
		me.init(me);
	},
	init: function(me) {
		Ext.QuickTips.init();
		me.panel = Ext.create('Skill.layout.Border');
		var viewport = new Ext.container.Viewport({
			layout: 'fit',
			items: [me.panel]
		});
	}
});