/**
 *
 * @author 朱雀
 */
Ext.define('SvgEditor.canvas.KeyMap', {
	//ctrl+c
	'ctrl+c': function(canvas) {
		canvas.copy(canvas);
	},
	//ctrl+v
	'ctrl+v': function(canvas) {
		canvas.paste(canvas);
	},
	//Delete
	'del': function(canvas) {
		canvas.removeModel(canvas);
	},
	'ctrl+a': function(canvas) {
		canvas.selectAll(canvas);
	}
});