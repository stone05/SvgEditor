/**
 *
 * @author 朱雀
 */
Ext.define('SvgEditor.canvas.Quickbar', {
	extend: 'Ext.toolbar.Toolbar',
	canvas: null,
	initComponent: function() {
		var me = this;
		me.initQuickBtn(me);
		me.items = me.quickBtn;
		me.callParent();
	},
	initQuickBtn: function(me) {
		me.copyBtn = Ext.create('Ext.button.Button', {
			name: 'copy',
			text: me.locale.canvasPanel.quickbar.copyBtn.text,
			tooltip: me.locale.canvasPanel.quickbar.copyBtn.qtip,
			canvas: me.canvas,
			handler: me.doOperation
		});
		me.pasteBtn = Ext.create('Ext.button.Button', {
			name: 'paste',
			text: me.locale.canvasPanel.quickbar.pasteBtn.text,
			tooltip: me.locale.canvasPanel.quickbar.pasteBtn.qtip,
			canvas: me.canvas,
			handler: me.doOperation
		});
		me.removeModelBtn = Ext.create('Ext.button.Button', {
			name: 'removeModel',
			text: me.locale.canvasPanel.quickbar.removeModelBtn.text,
			tooltip: me.locale.canvasPanel.quickbar.removeModelBtn.qtip,
			canvas: me.canvas,
			handler: me.doOperation
		});
		me.addKnuckleBtn = Ext.create('Ext.button.Button', {
			name: 'addKnuckle',
			text: me.locale.canvasPanel.quickbar.addKnuckleBtn.text,
			tooltip: me.locale.canvasPanel.quickbar.addKnuckleBtn.qtip,
			canvas: me.canvas,
			handler: me.doOperation
		});
		me.removeKnuckleBtn = Ext.create('Ext.button.Button', {
			name: 'removeKnuckle',
			text: me.locale.canvasPanel.quickbar.removeKnuckleBtn.text,
			tooltip: me.locale.canvasPanel.quickbar.removeKnuckleBtn.qtip,
			canvas: me.canvas,
			handler: me.doOperation
		});
		me.quickBtn = [me.copyBtn, me.pasteBtn, me.removeModelBtn, me.addKnuckleBtn, me.removeKnuckleBtn];
	},
	doOperation: function() {
		var operation = this.name, canvas = this.canvas;
		canvas[operation](canvas);
	}
});