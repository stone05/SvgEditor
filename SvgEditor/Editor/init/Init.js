/**
 *
 * @author 朱雀
 */
Ext.define('SvgEditor.init.Init', {
	mixins: {
		initWork: 'SvgEditor.init.InitWork'
	},
	constructor: function(config) {
		var me = this;
		if (config) {
			Ext.apply(me, config);
		}
	},
	editor: null,
	init: function() {
		var me = this, editor = me.editor, canvas = editor.ep.canvas;
		me.mixins.initWork.init(canvas);
	}
});