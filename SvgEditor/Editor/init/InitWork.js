/**
 *
 * @author 朱雀
 */
Ext.define('SvgEditor.init.InitWork', {
	init: function(canvas) {
		var startConfig = {
			modelId: 'start',
			type: 'circle',
			attrs: {
				cx: 100, cy: 200, r: 20,
				fill: '#00ff33', 'fill-opacity': 1
			}
		}, endConfig = {
			modelId: 'end',
			type: 'circle',
			attrs: {
				cx: canvas.paper.width - 100, cy: 200, r: 20,
				fill: 'red', 'fill-opacity': 1
			}
		};
		;
		canvas.start = canvas.createModel(canvas, startConfig);
		canvas.end = canvas.createModel(canvas, endConfig);
	}
});