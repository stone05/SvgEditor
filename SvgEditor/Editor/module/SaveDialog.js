/**
 *
 * @author 朱雀
 */
Ext.define('SvgEditor.module.SaveDialog', {
	extend: 'Ext.window.Window',
	layout: 'fit',
	width: 400,
	height: 300,
	buttonAlign: 'center',
	closable: false,
	initComponent: function() {
		var me = this;
		me.initForm(me);
		me.initBtns(me);
		me.items = [me.form];
		me.callParent();
	},
	initForm: function(me) {
		me.modelName = Ext.create('Ext.form.field.Text',
				{
					xtype: 'textfield',
					name: 'modelName',
					fieldLabel: '模型名称',
					anchor: '100%'
				});
		me.modelDescribe = Ext.create('Ext.form.field.TextArea', {
			name: 'modelDescribe',
			labelAlign: 'top',
			fieldLabel: '模型描述',
			anchor: '100% -50'
		});
		me.form = Ext.create('Ext.form.Panel', {
			fieldDefaults: {
				labelWidth: 60,
				margin: '10 10 0 10'
			},
			items: [me.modelName, me.modelDescribe]
		});
	},
	initBtns: function(me) {
		me.buttons = [
			{
				text: '确认'
			},
			{
				text: '取消',
				handler: function() {
					me.close();
				}
			}
		];
	}
});