/**
 *
 * @author 朱雀
 */
Ext.define('SvgEditor.module.TopToolbar', {
	extend: 'Ext.toolbar.Toolbar',
	canvas: null,
	initUtil: null,
	locale: null,
	initComponent: function() {
		var me = this;
		me.items = [
			{
				iconCls: 'SaveButton',
				tooltip: me.locale.topToolbar.saveBtn.qtip,
				handler: function() {
					var canvas = me.canvas, diagram = canvas.getDiagram(),
							saveDialog = Ext.create('SvgEditor.module.SaveDialog');
					diagram = {diagram: diagram};
					var diagramJSON = Ext.encode(diagram);
					saveDialog.modelDescribe.setValue(diagramJSON);
					saveDialog.show();
				}
			},
			{
				text: me.locale.topToolbar.openBtn.text,
				iconCls: '',
				tooltip: me.locale.topToolbar.openBtn.qtip,
				handler: function() {
					var canvas = me.canvas;
					canvas.initCanvas(canvas);
					var diagramJSON =
							'{"diagram":[{"modelId":"start","type":"circle","attrs":{"cx":100,"cy":200,"r":20,"fill":"#00ff33","stroke":"#000","fill-opacity":1},"data":{"text":"start"},"text":{"type":"text","attrs":{"x":100,"y":230,"text-anchor":"middle","text":"start","font":"14px \u5b8b\u4f53","stroke":"none","fill":"#000"}},"flowFromId":[],"flowToId":["path-3","path-8","path-9"]},{"modelId":"end","type":"circle","attrs":{"cx":915,"cy":200,"r":20,"fill":"red","stroke":"#000","fill-opacity":1},"data":{"text":"end"},"text":{"type":"text","attrs":{"x":914.9999999999999,"y":230,"text-anchor":"middle","text":"end","font":"14px \u5b8b\u4f53","stroke":"none","fill":"#000"}},"flowFromId":["path-10","path-11","path-12"],"flowToId":[]},{"modelId":"image-2","type":"image","attrs":{"x":423,"y":67,"width":102,"height":82,"src":"model/view/activity/usertask.svg","fill":"white"},"data":{"a":"a1","b":"b2","c":"c3","text":"\u4efb\u52a11"},"text":{"type":"text","attrs":{"x":474,"y":159,"text-anchor":"middle","text":"\u4efb\u52a11","font":"14px \u5b8b\u4f53","stroke":"none","fill":"#000"}},"flowFromId":["path-3"],"flowToId":["path-5","path-12"]},{"modelId":"path-3","type":"path","attrs":{"fill":"none","stroke":"black","path":[["M",121.36314290740077,194.74489532759125],["L",421,121]],"stroke-width":2.5,"arrow-end":"classic-wide-long"},"data":{},"flowFromId":"start","flowToId":"image-2"},{"modelId":"image-4","type":"image","attrs":{"x":421,"y":244,"width":102,"height":82,"src":"model/view/activity/usertask.svg","fill":"white"},"data":{"a":"a1","b":"b2","c":"c3","text":"\u4efb\u52a12"},"text":{"type":"text","attrs":{"x":472,"y":336,"text-anchor":"middle","text":"\u4efb\u52a12","font":"14px \u5b8b\u4f53","stroke":"none","fill":"#000"}},"flowFromId":["path-5","path-8"],"flowToId":["path-7","path-11"]},{"modelId":"path-5","type":"path","attrs":{"fill":"none","stroke":"black","path":[["M",473,151],["L",473,242]],"stroke-width":2.5,"arrow-end":"classic-wide-long"},"data":{},"flowFromId":"image-2","flowToId":"image-4"},{"modelId":"image-6","type":"image","attrs":{"x":421,"y":436,"width":102,"height":82,"src":"model/view/activity/usertask.svg","fill":"white"},"data":{"a":"a1","b":"b2","c":"c3","text":"\u4efb\u52a13"},"text":{"type":"text","attrs":{"x":472,"y":528,"text-anchor":"middle","text":"\u4efb\u52a13","font":"14px \u5b8b\u4f53","stroke":"none","fill":"#000"}},"flowFromId":["path-7","path-9"],"flowToId":["path-10"]},{"modelId":"path-7","type":"path","attrs":{"fill":"none","stroke":"black","path":[["M",472,328],["L",472,434]],"stroke-width":2.5,"arrow-end":"classic-wide-long"},"data":{},"flowFromId":"image-4","flowToId":"image-6"},{"modelId":"path-8","type":"path","attrs":{"fill":"none","stroke":"black","path":[["M",121.44724490829546,204.90058015377718],["L",419,272]],"stroke-width":2.5,"arrow-end":"classic-wide-long"},"data":{},"flowFromId":"start","flowToId":"image-4"},{"modelId":"path-9","type":"path","attrs":{"fill":"none","stroke":"black","path":[["M",117.64543187171391,213.13920599049666],["L",419,437]],"stroke-width":2.5,"arrow-end":"classic-wide-long"},"data":{},"flowFromId":"start","flowToId":"image-6"},{"modelId":"path-10","type":"path","attrs":{"fill":"none","stroke":"black","path":[["M",525,444],["L",896.3464032824519,211.66376137869258]],"stroke-width":2.5,"arrow-end":"classic-wide-long"},"data":{},"flowFromId":"image-6","flowToId":"end"},{"modelId":"path-11","type":"path","attrs":{"fill":"none","stroke":"black","path":[["M",525,275],["L",893.3941206967979,204.1455976089665]],"stroke-width":2.5,"arrow-end":"classic-wide-long"},"data":{},"flowFromId":"image-4","flowToId":"end"},{"modelId":"path-12","type":"path","attrs":{"fill":"none","stroke":"black","path":[["M",527,120],["L",893.4636504104661,195.50715609470043]],"stroke-width":2.5,"arrow-end":"classic-wide-long"},"data":{},"flowFromId":"image-2","flowToId":"end"}]}';
					if (diagramJSON) {
						var diagram = Ext.decode(diagramJSON).diagram;
						canvas.setDiagram(canvas, diagram);
					}
				}
			},
			{
				text: me.locale.topToolbar.resetBtn.text,
				iconCls: '',
				tooltip: me.locale.topToolbar.resetBtn.qtip,
				handler: function() {
					var canvas = me.canvas;
					canvas.initCanvas(canvas);
					if (me.initUtil) {
						me.initUtil.init();
					}
				}
			},
			{
				text: me.locale.topToolbar.clearBtn.text,
				iconCls: '',
				tooltip: me.locale.topToolbar.clearBtn.qtip,
				handler: function() {
					var canvas = me.canvas;
					canvas.initCanvas(canvas);
				}
			}
		];
		me.callParent();
	}
});