/**
 *
 * @author 朱雀
 */
Ext.define('SvgEditor.module.EditorPanel', {
	extend: 'Ext.panel.Panel',
	layout: 'border',
	readOnly: false,
	locale: null,
	util: null,
	initComponent: function() {
		var me = this;
		me.initCanvas(me);
		me.initPropertiesPanel(me);
		me.canvas.propertiesPanel = me.propertiesPanel;
		me.propertiesPanel.canvas = me.canvas;
		me.initModelTree(me);
		me.canvas.treeStore = me.propertiesPanel.treeStore = me.modelTree.store;
		me.modelTree.propertiesPanel = me.propertiesPanel;
		me.initTbar(me);
		me.items = [me.modelTree, me.canvas, me.propertiesPanel];
		me.callParent();
	},
	initModelTree: function(me) {
		me.modelTree = Ext.create('SvgEditor.module.ModelTree', {
			region: 'west',
			width: 200,
			util: me.util,
			locale: me.locale
		});
	},
	initPropertiesPanel: function(me) {
		me.propertiesPanel = Ext.create('SvgEditor.module.PropertiesPanel', {
			region: 'east',
			width: 300,
			locale: me.locale
		});
	},
	initCanvas: function(me) {
		me.canvas = Ext.create('SvgEditor.module.CanvasPanel', {
			region: 'center',
			readOnly: me.readOnly,
			locale: me.locale,
			util: me.util
		});
	},
	initTbar: function(me) {
		me.tbar = me.topBar = Ext.create('SvgEditor.module.TopToolbar', {
			canvas: me.canvas,
			locale: me.locale
		});
	}
});